package com.artivisi.training.microservice201901.frontend.controller;

import com.artivisi.training.microservice201901.frontend.service.NasabahClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class NasabahController {

    @Autowired private NasabahClient nasabahClient;

    @GetMapping("/nasabah/list")
    public ModelMap dataNasabah() {
        return new ModelMap()
                .addAttribute("dataNasabah", nasabahClient.semuaNasabah());
    }
}
