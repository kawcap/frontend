package com.artivisi.training.microservice201901.frontend.service;

import com.artivisi.training.microservice201901.frontend.dto.Nasabah;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient("nasabah")
public interface NasabahClient {

    @GetMapping("/nasabah/semua")
    Iterable<Nasabah> semuaNasabah();
}
